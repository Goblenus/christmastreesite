<?php
header('Content-Type: text/event-stream');
header('Cache-Control: no-cache');
/*print(file_get_contents('Added.txt'));*/

require_once("Settings.php");

$link = mysql_connect($dbIp, $dbUser, $dbPassword);

if (!$link) 
	return;

if (!mysql_select_db($dbDataBaseName)) 
{
    mysql_close($link);
    return;
}
    
$query = "Select Message, X, Y, ToyIdentifier, Type from AddedChristmasToys";
            
$result = mysql_query($query);

if (!$result or mysql_num_rows($result) == 0)
{
    mysql_close($link);
    return;
}

$jsonArray = array();
    
while ($row = mysql_fetch_assoc($result)) 
{
    $row["Message"] = base64_encode($row["Message"]);
    array_push($jsonArray, $row);
}

print(json_encode($jsonArray));
    
mysql_close($link);
?>