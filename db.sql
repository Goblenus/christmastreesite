CREATE TABLE `AddedChristmasToys` (
  `pk` int(11) NOT NULL AUTO_INCREMENT,
  `Message` varchar(255) DEFAULT NULL,
  `X` int(11) DEFAULT NULL,
  `Y` int(11) DEFAULT NULL,
  `ToyIdentifier` varchar(100) DEFAULT NULL,
  `Type` int(11) DEFAULT NULL,
  PRIMARY KEY (`pk`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

CREATE TABLE `ChristmasToys` (
  `pk` int(11) NOT NULL AUTO_INCREMENT,
  `Message` varchar(255) DEFAULT NULL,
  `X` int(11) DEFAULT NULL,
  `Y` int(11) DEFAULT NULL,
  `ToyIdentifier` varchar(100) DEFAULT NULL,
  `Type` int(11) DEFAULT NULL,
  PRIMARY KEY (`pk`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;