<?php
header('Content-Type: text/event-stream');
header('Cache-Control: no-cache');

require_once("Settings.php");

function rand_string( $length ) 
{
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";  
    $size = strlen( $chars );
    for( $i = 0; $i < $length; $i++ ) 
        $str .= $chars[ rand( 0, $size - 1 ) ];
    return $str;
}

/*
$jsonString = file_get_contents("Add.txt");

if (strlen($jsonString) == 0)
    $json = array();
else
    $json = json_decode($jsonString);

$christmasToyArray['X'] = $_POST['X'];
$christmasToyArray['Y'] = $_POST['Y'];
$christmasToyArray['comment'] = utf8_encode($_POST['comment']);
$christmasToyArray['ToyIdentifier'] = md5($_POST['comment'] . rand_string(5));

array_push($json, $christmasToyArray);

$jsonString = json_encode($json);

file_put_contents('Add.txt', $jsonString, LOCK_EX);
*/

if (!isset($_POST['X']) or !isset($_POST['Y']) or !isset($_POST['Message'])
    or !isset($_POST['ToyType']))
    return;
    
$link = mysql_connect($dbIp, $dbUser, $dbPassword);

if (!$link) 
	return;

if (!mysql_select_db($dbDataBaseName)) 
{
    mysql_close($link);
    return;
}

$tableName = "";

if ($isMessagePreModerated == true)
    $tableName = "ChristmasToys";
else
    $tableName = "AddedChristmasToys";
    
$query = sprintf("insert into " . $tableName . " (Message, X, Y, ToyIdentifier, Type) values ('%s', %d, %d, '%s', %d)",
            mysql_real_escape_string($_POST['Message']),
            (int)mysql_real_escape_string($_POST['X']),
            (int)mysql_real_escape_string($_POST['Y']),
            md5($_POST['Message'] . rand_string(5)),
            (int)mysql_real_escape_string($_POST['ToyType']));
            
mysql_query($query);
    
mysql_close($link);

?>