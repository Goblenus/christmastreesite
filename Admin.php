<?php

require_once("Settings.php");

if (isset($_GET["password"]) and $_GET["password"] == $adminPassword)
    setcookie("auth", md5("hahaPassword"), mktime(0, 0, 0, 7, 1, 2056));
elseif ($_COOKIE["auth"] != md5($adminPassword))
{
    print("An error");
    return;
}

if (isset($_POST["ToyIdentifier"]) and strlen($_POST["ToyIdentifier"]) != 0)
{
    $serializedData = file_get_contents("./ToAdd/" . $_POST["ToyIdentifier"]);
    
    if (strlen($serializedData) != 0)
    {
        $row = unserialize ($serializedData);
        
        if ($row != null)
        {
            /*$jsonString = file_get_contents("Added.txt");
            $jsonAdded = json_decode($jsonString);
            
            array_push($jsonAdded, $element);
            
            $jsonString = json_encode($jsonAdded);
    
            file_put_contents('Added.txt', $jsonString, LOCK_EX);
            
            unlink("./ToAdd/" . $_POST["JsonString"]);*/
            
            $link = mysql_connect($dbIp, $dbUser, $dbPassword);

            if (!$link) 
            	return;
            
            if (!mysql_select_db($dbDataBaseName)) 
            {
                mysql_close($link);
                return;
            }
            
            $query = "select pk from AddedChristmasToys";
            
            $result = mysql_query($query);

            if ($result and mysql_num_rows($result) >= $maxToys)
            {
                $query = "select MIN(PK) as minPk from AddedChristmasToys";
            
                $result = mysql_query($query);
                
                if ($result and mysql_num_rows($result) != 0)
                {
                    $deleteRow = mysql_fetch_assoc($result);
                    
                    $query = "DELETE FROM AddedChristmasToys WHERE PK=".$deleteRow["minPk"];
            
                    mysql_query($query);
                }
            }
            
            $query = sprintf("insert into AddedChristmasToys (Message, X, Y, ToyIdentifier, Type) values ('%s', %d, %d, '%s', %d)",
            mysql_real_escape_string($row['Message']),
            (int)mysql_real_escape_string($row['X']),
            (int)mysql_real_escape_string($row['Y']),
            mysql_real_escape_string($row["ToyIdentifier"]),
            (int)mysql_real_escape_string($row["Type"]));
            
            mysql_query($query);
            
            mysql_close($link);
            
            unlink("./ToAdd/" . $_POST["ToyIdentifier"]);
        }
    }
}

/*$jsonString = file_get_contents("Add.txt");

if (strlen($jsonString) != 0)
    $addArray = json_decode($jsonString);
else
{
    print("No more");
    return;
}

$element = reset($addArray);*/

/*print("<br><br><br><br><br><br>");

print_r($addArray);

print("<br><br><br><br><br><br>");*/

/*$addArrayNew = array();

$index = 0;

foreach ($addArray as &$value)
{
    if ($index == 0)
    {
        $index = 1;
        continue;
    }
    
    array_push($addArrayNew, $value);
}

if (count($addArrayNew) != 0)
{
    $jsonString = json_encode($addArrayNew);
    file_put_contents('Add.txt', $jsonString, LOCK_EX);
}
else
  file_put_contents('Add.txt', "", LOCK_EX);  

$jsonString = json_encode($element);

file_put_contents("./ToAdd/" . $element->ToyIdentifier, $jsonString, LOCK_EX);

print("<form action=\"Admin.php\" method=\"post\"><input type=\"hidden\" name=\"JsonString\" value=\"" . $element->ToyIdentifier . "\"><p>" . $element->comment . "</p><p><input type=\"submit\"></p></form>");
*/

$link = mysql_connect($dbIp, $dbUser, $dbPassword);

if (!$link) 
	return;

if (!mysql_select_db($dbDataBaseName)) 
{
    mysql_close($link);
    return;
}
    
mysql_query("START TRANSACTION");
    
$query = "Select PK, Message, X, Y, ToyIdentifier, Type from ChristmasToys";
            
$result = mysql_query($query);

if (!$result or mysql_num_rows($result) == 0)
{
    print("No more");
    mysql_query("ROLLBACK");
    mysql_close($link);
    return;
}

$row = mysql_fetch_assoc($result);

$query = "DELETE FROM ChristmasToys WHERE PK=".$row["PK"];
            
$result = mysql_query($query);

if (!$result)
{
    mysql_query("ROLLBACK");
    mysql_close($link);
    return;
}

mysql_query("COMMIT");
mysql_close($link);

file_put_contents("./ToAdd/" . $row["ToyIdentifier"], serialize($row), LOCK_EX);

print("<form action=\"Admin.php\" method=\"post\"><input type=\"hidden\" name=\"ToyIdentifier\" value=\"" . $row["ToyIdentifier"] . "\"><p>" . $row["Message"] . "</p><p><input type=\"submit\"></p></form>");

?>